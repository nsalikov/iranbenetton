# -*- coding: utf-8 -*-

import urllib2
import base64
from magento import MagentoAPI
import decimal
import re
import json

import requests
from requests.packages.urllib3.util.retry import Retry
from requests.adapters import HTTPAdapter


class ManegntoClothesProduct():
    def __init__(self, config):
        self.api = MagentoAPI(config['api']['endpoint'], 80, config['api']['user'], config['api']['key'], path=config['api']['path'])
        self.sku = None
        self.product_id = None
        self.size_id = None
        self.attributes_set_id = 9
        self.url = ''
        self.url_key = ''
        self.status = 1
        self.manufacturer = self.get_attr_id('manufacturer', config['manufacter'])
        self.color = ''
        self.name = ''
        self.description = ''
        self.short_description = ''
        self.websites = [1]
        self.price = ''
        self.special_price = ''
        self.orig_sizes = []
        self.categories = []
        self.sizes = []
        self.store_view = 1
        self.magento_product = None
        self.related_colors = ''

        self.updated = False
        self.created = False
        self.loaded = False

        self.sz = {}
        self.sz['XSS'] = 0
        self.sz['XS'] = 1
        self.sz['S'] = 2
        self.sz['M'] = 3
        self.sz['L'] = 4
        self.sz['XL'] = 5
        self.sz['XXL'] = 6
        self.sz['XXXL'] = 7

        self.update_log = []

    def size_to_num(self, size):
        if size in self.sz:
            return self.sz[size]

        found = re.search('([0-9]+)[^0-9]([0-9]+)', size)

        if found and len(found.groups()) == 2:
            return int(found.group(1)) * 100 + int(found.group(2))

        return size

    def size_sorter(self, a, b):
        an = self.size_to_num(a)
        bn = self.size_to_num(b)
        if an > bn:
            return 1
        elif an == bn:
            return 0
        else:
            return -1

    def get_category_id(self, categories):
        current_id = 2
        for category in categories:
            list = self.api.catalog_category.level(1, 1, current_id)

            tmp = 0
            for cat in list:
                if cat['name'] == category:
                    tmp = cat['category_id']

            if tmp:
                current_id = tmp
            else:
                current_id = self.api.catalog_category.create(current_id, {'name': category, 'is_active': 1,
                                                                           'available_sort_by': 'position',
                                                                           'default_sort_by': 'position',
                                                                           'include_in_menu': 1})

        return current_id

    def get_attr_id(self, attr_name, attr_value):
        if not attr_value:
            raise ValueError('attr_value is null for {}'.format(attr_name))

        info = self.api.catalog_product_attribute.info(attr_name)
        if 'options' in info:
            for option in info['options']:
                if option['label'] == attr_value:
                    return option['value']

        new_attr = {'label': [{'store_id': 0, 'value': attr_value}]}
        self.api.catalog_product_attribute.addOption(attr_name, new_attr)

        return self.get_attr_id(attr_name, attr_value)

    def _get_dict(self):
        details = dict()

        details['url'] = self.url
        details['url_key'] = self.url_key
        details['status'] = self.status
        details['manufacturer'] = self.manufacturer
        details['websites'] = [1]
        details['name'] = self.name

        details['description'] = self.description
        details['short_description'] = self.short_description

        details['price'] = self.price
        details['related_colors'] = self.related_colors

        if self.color:
            details['color'] = self.get_attr_id('color', self.color)

        return details

    def is_same_price(self, price_name):
        if price_name not in self.__dict__:
            return False

        if price_name not in self.magento_product:
            return False

        if not self.__dict__[price_name]:
            return False

        if not self.magento_product[price_name]:
            return False

        if decimal.Decimal(self.__dict__[price_name].replace(",", ".")) == decimal.Decimal(
                self.magento_product[price_name].replace(",", ".")):
            return True
        else:
            return False

    def load(self, sku):
        sku = sku.strip() + ' '
        self.sku = sku

        try:
            self.magento_product = self.api.catalog_product.info(self.sku)
        except Exception as e:
            pass

        if self.magento_product:
            self.loaded = True
            self.product_id = self.magento_product['product_id']
            self.price = self.magento_product['price']
            self.special_price = self.magento_product['special_price']
            self.size_id = self.get_option_id('Size')
            self.orig_sizes = self.get_sizes()
            self.sizes = list(self.orig_sizes)
            self.status = self.magento_product['status']
            return True
        else:
            return False

    def save(self):
        if self.magento_product:
            self.update()
        else:
            self.create()

    def remove_sizes(self):
        if self.size_id:
            self.api.catalog_product_custom_option.remove(self.size_id)

    def update(self):
        if self.price is None:
            self.price = ''

        if self.special_price is None:
            self.special_price = ''

        if self.price.count('.') > 1:
            self.price.replace('.', '', 1)

        if not self.is_same_price('price'):
            self.api.catalog_product.update(self.sku, {'price': self.price})
            self.update_log.append({'field': 'price', 'old': self.magento_product['price'], 'new': self.price})

        if self.special_price.count('.') > 1:
            self.special_price.replace('.', '', 1)

        if not self.is_same_price('special_price'):
            self.api.catalog_product.update(self.sku, {'special_price': self.special_price})
            self.update_log.append(
                {'field': 'special_price', 'old': self.magento_product['special_price'], 'new': self.special_price})

        if int(self.magento_product['status']) != int(self.status):
            # add log
            if self.status == 1:
                self.update_log.append({'field': 'status', 'old': 'disabled', 'new': 'enabled'})
            else:
                self.update_log.append({'field': 'status', 'old': 'enabled', 'new': 'disabled'})
            self.api.catalog_product.update(self.sku, {'status': self.status})

        if set(self.orig_sizes) != set(self.sizes):
            self.remove_sizes()
            self.add_sizes(self.magento_product['product_id'])
            if set(self.orig_sizes) != set(self.sizes):
                self.update_log.append(
                    {'field': 'size', 'old': ", ".join(self.orig_sizes), 'new': ",".join(self.sizes)})

        if self.categories:
            new_category = self.get_category_id(self.categories)
            self.api.catalog_category.assignProduct(new_category, self.product_id)

        if self.related_colors:
            self.api.catalog_product.update(self.sku, {'related_colors': self.related_colors})

        self.updated = True

    def get_sizes(self):
        sizes = None

        try:
            sizes = self.api.catalog_product_custom_option.info(self.size_id)
        except Exception as e:
            pass

        result = []
        if sizes:
            for size in sizes['additional_fields']:
                result.append(size['title'])
        return result

    def get_option_id(self, title):
        options = self.api.catalog_product_custom_option.list(self.magento_product['product_id'])
        for option in options:
            if option['title'] == title:
                return option['option_id']

        return 0

    def create(self):
        details = self._get_dict()
        self.product_id = self.api.catalog_product.create('simple', self.attributes_set_id, self.sku, details)
        self.api.catalog_product.setSpecialPrice(self.sku, self.special_price)

        new_category = self.get_category_id(self.categories)
        self.api.catalog_category.assignProduct(new_category, self.product_id)
        self.add_sizes(self.product_id)
        self.created = True

    def add_sizes(self, id):
        custom_field = dict()

        sizes = []
        custom_field['title'] = 'Size';
        custom_field['type'] = 'drop_down'
        custom_field['is_require'] = 1
        custom_field['additional_fields'] = []

        i = 0

        new_sizes = list(set(self.sizes) | set(self.orig_sizes))
        new_sizes.sort(self.size_sorter)

        for size in new_sizes:
            sizes.append(size)
            size_item = dict()
            size_item['title'] = size
            size_item['price_type'] = 'fixed'
            size_item['sort_order'] = i
            i += 1

            custom_field['additional_fields'].append(size_item)

        self.sizes = sizes
        if len(custom_field['additional_fields']) > 0:
            self.api.catalog_product_custom_option.add(id, custom_field)

    def add_image(self, image_url, first_image=False):
        if image_url.startswith("//"):
            image_url = 'https:' + image_url

        img = self._get_image(image_url)
        if not img:
            return

        image = dict()
        image['file'] = dict();
        image['file']['name'] = 'image'
        image['file']['content'] = img
        image['file']['mime'] = 'image/jpeg'
        image['exclude'] = 0
        if first_image:
            image['types'] = ['image', 'small_image', 'thumbnail']

        try:
            self.api.catalog_product_attribute_media.create(self.sku, image)
        except Exception as e:
            if str(e) == "<Fault 104: 'Unsupported image format.'>":
                print 'invalid image'
            else:
                raise

    def _get_image(self, image_url):
        img = None

        s = requests.Session()
        retries = Retry(total=3, backoff_factor=1, status_forcelist=[500, 502, 503, 504])
        s.mount('http://', HTTPAdapter(max_retries=retries))
        s.mount('https://', HTTPAdapter(max_retries=retries))

        if image_url.startswith("/"):
            with open(image_url, "rb") as image_file:
                img = base64.b64encode(image_file.read())
        else:
            try:
                response = s.get(image_url, timeout=60)
                img = base64.b64encode(response.content)
            except:
                pass

        return img

    def set_types_for_first_image(self):
        mlist = self.api.catalog_product_attribute_media.list(self.product_id)
        if len(mlist) > 0:
            if len(mlist[0]['types']) == 0:
                self.api.catalog_product_attribute_media.update(self.product_id, mlist[0]['file'],
                                                                {'types': ['image', 'small_image', 'thumbnail']})

    def images_count(self):
        img_list = self.api.catalog_product_attribute_media.list(self.product_id)
        return len(img_list)

    def remove_all_images(self):
        img_list = self.api.catalog_product_attribute_media.list(self.product_id)
        for img in img_list:
            self.api.catalog_product_attribute_media.remove(self.product_id, img['file'])

    def enable(self):
        self.status = 1

    def disable(self):
        self.status = 2

    def get_all_products(self):
        return self.api.catalog_product.list(self.store_view)

    def export(self, data):
        if 'sku' in data and data['sku']:
            data['sku'] = data['sku'].strip() + ' '
        else:
            return

        self.load(data['sku'])

        if 'url' in data and data['url']:
            self.url = data['url']
        if 'sku' in data and data['sku']:
            self.url_key = data['sku']
        if 'name' in data and data['name']:
            self.name = data['name']
        if 'price' in data and data['price']:
            self.price = data['price']
        if 'color' in data and data['color']:
            self.color = data['color']
        if 'special_price' in data and data['special_price']:
            self.special_price = data['special_price']
        if 'description' in data and data['description']:
            self.description = data['description']
        if 'short_description' in data and data['short_description']:
            self.short_description = data['short_description']
        if 'sizes' in data and data['sizes']:
            self.sizes = data['sizes']
        if 'categories' in data and data['categories']:
            self.categories = data['categories']

        if 'related_colors' in data and data['related_colors']:
            if isinstance(data['related_colors'], str):
                self.related_colors = data['related_colors']
            else:
                self.related_colors = json.dumps(data['related_colors'])

        if 'related_skus' in data and data['related_skus']:
            if isinstance(data['related_skus'], str):
                self.related_colors = data['related_skus']
            else:
                self.related_colors = json.dumps(data['related_skus'])

        if 'out_of_stock' in data:
            if data['out_of_stock'] == 'yes':
                self.disable()
            else:
                self.enable()

        self.save()
