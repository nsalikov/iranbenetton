# -*- coding: utf-8 -*-

import dependency_injector.containers as containers
import dependency_injector.providers as providers

from scraper.ioc.core import core

from scraper.models_factory import ModelsFactory

storage = containers.DynamicContainer()
storage.factory = providers.Singleton(ModelsFactory, config=core.config.storage.db)
