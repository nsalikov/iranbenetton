# -*- coding: utf-8 -*-

import dependency_injector.containers as containers
import dependency_injector.providers as providers

from scraper.list import ListHandler
from scraper.ioc.storage import storage
from scraper.ioc.core import core

list = containers.DynamicContainer()

list.list_koton = providers.Singleton(ListHandler, db_factory=storage.factory, config=core.config.export.koton)
list.list_lcwaikiki = providers.Singleton(ListHandler, db_factory=storage.factory, config=core.config.export.lcwaikiki)
list.list_uspoloassn = providers.Singleton(ListHandler, db_factory=storage.factory, config=core.config.export.uspoloassn)
list.list_defacto = providers.Singleton(ListHandler, db_factory=storage.factory, config=core.config.export.defacto)
list.list_mango = providers.Singleton(ListHandler, db_factory=storage.factory, config=core.config.export.mango)
list.list_hm = providers.Singleton(ListHandler, db_factory=storage.factory, config=core.config.export.hm)
