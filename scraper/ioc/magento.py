# -*- coding: utf-8 -*-

import dependency_injector.containers as containers
import dependency_injector.providers as providers

from scraper.ioc.core import core

from scraper.magento_clothes_product import ManegntoClothesProduct
from scraper.magento_estate_product import ManegntoEstateProduct

magento = containers.DynamicContainer()
magento.product_koton = providers.Factory(ManegntoClothesProduct, config=core.config.export.koton)
magento.product_lcwaikiki = providers.Factory(ManegntoClothesProduct, config=core.config.export.lcwaikiki)
magento.product_uspoloassn = providers.Factory(ManegntoClothesProduct, config=core.config.export.uspoloassn)
magento.product_defacto = providers.Factory(ManegntoClothesProduct, config=core.config.export.defacto)
magento.product_mango = providers.Factory(ManegntoClothesProduct, config=core.config.export.mango)
magento.product_hm = providers.Factory(ManegntoClothesProduct, config=core.config.export.hm)
magento.product_zingat = providers.Factory(ManegntoEstateProduct, config=core.config.export.zingat)
