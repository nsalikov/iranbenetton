# -*- coding: utf-8 -*-

import dependency_injector.containers as containers
import dependency_injector.providers as providers

import scraper.config_loader

config_loaded = False

core = containers.DynamicContainer()
core.config = providers.Configuration('config')


def load_config(filename='config.yml'):
    global config_loaded
    if config_loaded:
        return
    config = scraper.config_loader.load_config(filename)
    core.config.update(config)
    config_loaded = True


core.load_config = load_config
