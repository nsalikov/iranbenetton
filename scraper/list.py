# -*- coding: utf-8 -*-

from pony.orm import db_session, select


class ListHandler:
    def __init__(self, db_factory, config):
        self.factory = db_factory
        self.config = config

    def add(self, list_name, value):
        try:
            with db_session:
                self.factory.get('List')(site_id=self.config['id'], list=list_name, payload=value)
            return True
        except Exception as e:
            # print e
            return False

    @db_session
    def get_all(self, list_name):
        return select(l.payload for l in self.factory.get('List') if l.list == list_name)[:]

