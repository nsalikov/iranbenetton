# -*- coding: utf-8 -*-

from datetime import datetime
from pony.orm import *
import inspect


class ModelsFactory:
    def __init__(self, config):
        self.db = Database()  # There is no sense with DI here, changing database outside will not work
        self.models = dict()
        db = self.db

        ############################
        class List(db.Entity):
            _table_ = 'lists'
            id = PrimaryKey(int, auto=True)
            site_id = Required(int)
            list = Required(unicode, index=True)
            payload = Required(unicode, index=True)
            composite_key(site_id, list, payload)

        class Product(db.Entity):
            _table_ = 'products'
            id = PrimaryKey(int, auto=True)
            site_id = Required(int)
            url = Optional(unicode, index=True)
            sku = Required(unicode)
            data = Required(LongStr)
            magento_id = Optional(int)
            field_logs = Set('FieldLog', cascade_delete=True)
            product_logs = Set('ProductLog', cascade_delete=True)
            composite_key(site_id, sku)

        class FieldLog(db.Entity):
            _table_ = 'field_logs'
            id = PrimaryKey(int, auto=True)
            product = Required(Product)
            field = Required(unicode)
            old_value = Optional(unicode)
            new_value = Optional(unicode)
            created_at = Required(datetime)

        class ProductLog(db.Entity):
            _table_ = 'product_logs'
            id = PrimaryKey(int, auto=True)
            product = Required(Product)
            status = Required(unicode)
            created_at = Required(datetime)

        ############################

        self.db.bind(config['driver'], host=config['host'], user=config['user'], passwd=config['password'],
                     db=config['database'],
                     charset=config['charset'])
        self.db.generate_mapping(create_tables=config['create_tables'])

        l = locals().copy()
        for name, obj in l.iteritems():
            if inspect.isclass(obj) and issubclass(obj, self.db.Entity):
                self.models[name] = obj

    def get(self, name):
        return self.models[name]
