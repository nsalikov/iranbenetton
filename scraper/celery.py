# -*- coding: utf-8 -*-

from __future__ import absolute_import
from scraper.ioc.core import core

core.load_config()

from celery import Task
from celery import Celery

import logging
from raven import Client
from raven.contrib.celery import register_signal, register_logger_signal

client = Client('http://2f10804087cd419ba2ce3707436953d9:d73c23bf2cf342d2b3bb33990f9e6ae2@46.4.78.38:9000/2')

# register a custom filter to filter out duplicate logs
register_logger_signal(client)

# The register_logger_signal function can also take an optional argument
# `loglevel` which is the level used for the handler created.
# Defaults to `logging.ERROR`
register_logger_signal(client, loglevel=logging.ERROR)

# hook into the Celery error handler
#register_signal(client)

# The register_signal function can also take an optional argument
# `ignore_expected` which causes exception classes specified in Task.throws
# to be ignored
register_signal(client, ignore_expected=True)

app = Celery(core.config.celery.app(),
             broker=core.config.celery.broker(),
             backend=core.config.celery.backend(),
             include=core.config.celery.include())

app.conf.update(core.config.celery.additional())

app.conf.task_routes = {
    'scraper.drivers.defacto.tasks.export_product': {'queue': 'defacto.export'},
    'scraper.drivers.defacto.tasks.add_images': {'queue': 'defacto.images'},
    'scraper.drivers.defacto.tasks.sync_product': {'queue': 'defacto.sync'},

    'scraper.drivers.koton.tasks.export_product': {'queue': 'koton.export'},
    'scraper.drivers.koton.tasks.add_images': {'queue': 'koton.images'},
    'scraper.drivers.koton.tasks.sync_product': {'queue': 'koton.sync'},

    'scraper.drivers.lcwaikiki.tasks.export_product': {'queue': 'lcwaikiki.export'},
    'scraper.drivers.lcwaikiki.tasks.add_images': {'queue': 'lcwaikiki.images'},
    'scraper.drivers.lcwaikiki.tasks.sync_product': {'queue': 'lcwaikiki.sync'},

    'scraper.drivers.uspoloassn.tasks.export_product': {'queue': 'uspoloassn.export'},
    'scraper.drivers.uspoloassn.tasks.add_images': {'queue': 'uspoloassn.images'},
    'scraper.drivers.uspoloassn.tasks.sync_product': {'queue': 'uspoloassn.sync'},

    'scraper.drivers.mango.tasks.export_product': {'queue': 'mango.export'},
    'scraper.drivers.mango.tasks.add_images': {'queue': 'mango.images'},
    'scraper.drivers.mango.tasks.sync_product': {'queue': 'mango.sync'},

    'scraper.drivers.hm.tasks.export_product': {'queue': 'hm.export'},
    'scraper.drivers.hm.tasks.add_images': {'queue': 'hm.images'},
    'scraper.drivers.hm.tasks.sync_product': {'queue': 'hm.sync'},

    'scraper.drivers.zingat.tasks.export_product': {'queue': 'zingat.export'},
    'scraper.drivers.zingat.tasks.add_images': {'queue': 'zingat.images'},
    'scraper.drivers.zingat.tasks.sync_product': {'queue': 'zingat.sync'},
}

class DatabaseTask(Task):
    abstract = True

    def __init__(self):
        pass
        # print self.request.kwargs
        # self.request.kwargs['_type'] = 'DatabaseTask'
        # self.run = my_decorator(self, self.run)

    # def on_failure(self, exc, task_id, args, kwargs, einfo):
    #
    #     client.user_context({
    #         'task_id': task_id
    #     })
    #
    #     client.captureException()
    #
    # def on_retry(self, exc, task_id, args, kwargs, einf):
    #
    #     client.user_context({
    #         'task_id': task_id
    #     })
    #
    #     client.captureException()


if __name__ == '__main__':
    app.start()
