# -*- coding: utf-8 -*-
# Config loader

import os
import sys
import yaml

# 1. Tries to load config from env
# 2. Tries to load filename in folder of main script
# 3. Tries to load from full path

_ENV_KEY = 'SCRAPER_CONFIG'


def get_config_filename(filename):
    if _ENV_KEY in os.environ:
        return os.environ[_ENV_KEY]

    if filename is None:
        raise ValueError("empty scraper config passed, and ENV['{}'] is None".format(_ENV_KEY))

    if not os.path.isfile(filename):
        directory = os.path.dirname(os.path.abspath(sys.modules['__main__'].__file__))
        path = os.path.join(directory, filename)
        if os.path.isfile(path):
            return path
        else:
            raise ValueError('unable to load scraper config: {}'.format(filename))
    return filename


def load_config(filename):
    filename = get_config_filename(filename)

    with open(filename) as data:
        return yaml.load(data)
