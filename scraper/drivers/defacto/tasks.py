# -*- coding: utf-8 -*-
from __future__ import absolute_import, unicode_literals

from scraper.ioc.core import core
from scraper.celery import app, client
from scraper.celery import DatabaseTask
from scraper.ioc.storage import storage
from scraper.ioc.magento import magento

from celery import group
from celery.utils.log import get_task_logger
import celery.exceptions
from pony.orm import db_session

import os
import json
from scraper.exceptions import DownloadError

factory = storage.factory()
logger = get_task_logger(__name__)


raven_enable = False


@app.task(base=DatabaseTask, autoretry_for=(Exception,), default_retry_delay=core.config.celery.autoretry_delay(), max_retries=None, bind=True)
def start_all(self, **kwargs):
    try:
        raise self.replace(export_products.si())
    except celery.exceptions.Ignore as e:
        raise e
    except Exception as e:
        if raven_enable:
            client.user_context({'task_id': self.request.id, 'kwargs': kwargs})
            client.captureException()
        raise e


@app.task(base=DatabaseTask, autoretry_for=(Exception,), default_retry_delay=core.config.celery.autoretry_delay(), max_retries=None, bind=True, throws=(DownloadError))
def export_products(self, **kwargs):
    try:
        tasks = []

        with db_session:
            args = {'site_id': core.config.export.defacto.id()}
            cursor = storage.factory().db.execute("select data from products where site_id = $site_id;", args)
            for t in cursor:
                item = json.loads(t[0])
                tasks.append(export_product.si(item))

        if tasks:
            raise self.replace(group(tasks))

        return
    except celery.exceptions.Ignore as e:
        raise e
    except DownloadError as e:
        raise e
    except Exception as e:
        if raven_enable:
            client.user_context({'task_id': self.request.id, 'kwargs': kwargs})
            client.captureException()
        raise e


@app.task(base=DatabaseTask, autoretry_for=(Exception,), default_retry_delay=core.config.celery.autoretry_delay(), max_retries=None, bind=True)
def export_product(self, item, **kwargs):
    try:
        product = magento.product_defacto()
        if not product:
            return

        product.export(item)

        with db_session:
            try:
                db_product = factory.get('Product').get(sku=item['sku'])
                db_product.magento_id = product.product_id

                for log in product.update_log:
                    db_product.field_logs.create(field=log['field'], old_value=log['old'], new_value=log['new'], created_at=datetime.now())

                status = 'updated'
                if product.created:
                    status = 'created'

                db_product.product_logs.create(status=status, created_at=datetime.now())
            except:
                pass

        if product.images_count() == 0:
            raise self.replace(add_images.si(item))
        else:
            return
    except celery.exceptions.Ignore as e:
        raise e
    except Exception as e:
        if raven_enable:
            client.user_context({'task_id': self.request.id, 'item': item, 'kwargs': kwargs})
            client.captureException()
        raise e


@app.task(base=DatabaseTask, autoretry_for=(Exception,), default_retry_delay=core.config.celery.autoretry_delay(), max_retries=None, bind=True, throws=(DownloadError))
def add_images(self, item, **kwargs):
    try:
        product = magento.product_defacto()
        product.load(item['sku'])

        images_path = '/home/dan/scrapyd/images'
        project_name = 'iranbenetton_spiders'
        spider_name = 'defacto'

        if 'images' in item:
            for img in item['images']:
                url = os.path.join(images_path, project_name, spider_name, img['path'])
                if not os.path.isfile(url):
                    print 'Unable to find ' + url
                    url = img['url']
                product.add_image(url)
        elif 'image_urls' in item:
            for url in item['image_urls']:
                product.add_image(url)

        product.set_types_for_first_image()
    except celery.exceptions.Ignore as e:
        raise e
    except Exception as e:
        if raven_enable:
            client.user_context({'task_id': self.request.id, 'item': item, 'kwargs': kwargs})
            client.captureException()
        raise e


@app.task(base=DatabaseTask, autoretry_for=(Exception,), default_retry_delay=core.config.celery.autoretry_delay(), max_retries=None, bind=True)
def sync_products(self, **kwargs):
    try:
        products = magento.product_defacto().get_all_products()
        if not products:
            return

        tasks = []
        for product in products:
            tasks.append(sync_product.si(sku=product['sku']))

        if tasks:
            raise self.replace(group(tasks))

        return
    except celery.exceptions.Ignore as e:
        raise e
    except Exception as e:
        if raven_enable:
            client.user_context({'task_id': self.request.id, 'kwargs': kwargs})
            client.captureException()
        raise e


@app.task(base=DatabaseTask, autoretry_for=(Exception,), default_retry_delay=core.config.celery.autoretry_delay(), max_retries=None, bind=True)
def sync_product(self, **kwargs):
    try:
        product = magento.product_defacto()
        product.load(kwargs['sku'])

        with db_session:
            if not factory.get('Product').exists(sku=kwargs['sku'], site_id=core.config.export.defacto.id()):
                product.disable()
                product.update()
    except celery.exceptions.Ignore as e:
        raise e
    except Exception as e:
        if raven_enable:
            client.user_context({'task_id': self.request.id, 'kwargs': kwargs})
            client.captureException()
        raise e
