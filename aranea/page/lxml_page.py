# -*- coding: utf-8 -*-
from __future__ import absolute_import, unicode_literals

from cssselect import GenericTranslator, SelectorError
import lxml
from lxml.etree import HTML, tostring
from itertools import chain
import codecs
import re

def stringify_children(node):
    return lxml.etree.tostring(node, pretty_print=False)


def itertext(self):
    tag = self.tag
    if not isinstance(tag, str) and tag is not None:
        return
    if self.text:
        yield self.text
    for e in self:
        if not isinstance(e, lxml.etree._Comment):
            for s in e.itertext():
                yield s
            if e.tail:
                yield e.tail


class Result(list):
    def __init__(self, *args, **kwargs):
        super(Result, self).__init__(args[0])

    def __getitem__(self, i):
        return super(Result, self).__getitem__(i)

    def empty(self):
        if len(self) > 0:
            return False
        else:
            return True

    def first(self):
        if not self.empty():
            return list.__getitem__(self, 0)
        else:
            return ''

    def fetch(self, regexp):
        result = Result([])
        for item in self:
            search = re.search(regexp, item, re.IGNORECASE)
            if search:
                result.append(search.group(1))


        return result


class LXMLPageHandler():
    def __init__(self, elements=None):
        self.elements = elements

    def update(self, html, url):
        self.html = html
        self.url = url
        self.elements = [HTML(html)]

    def link_fetcher(self, fetcher):
        pass

    def getUrl(self):
        return self.url

    def getElements(self):
        return self.elements

    def __iter__(self):
        return iter(self.getPageElements())

    def getPageElements(self):
        result = []
        for element in self.getElements():
            result.append(LXMLPageHandler([element]))

        return result

    def get(self, index):
        if len(self.getElements()) > 0:
            return LXMLPageHandler([self.elements[index]])
        else:
            pass  # TODO: Error?

    def length(self):
        return len(self.getElements())

    def exists(self, t_val=True, f_val=False):
        if self.length() > 0:
            return t_val
        else:
            return f_val

    def click(self):
        raise NotImplementedError

    def mouse_move(self):
        raise NotImplementedError

    def _select(self, selector_type, selector):
        result = []
        if selector_type == 'CSS_SELECTOR':
            selector = GenericTranslator().css_to_xpath(selector)

        for element in self.getElements():
            result += element.xpath(selector)

        return LXMLPageHandler(result)

    def xpath(self, selector):
        return self._select('XPATH', selector)

    def css(self, selector):
        return self._select('CSS_SELECTOR', selector)

    def text(self):
        result = Result([])
        for element in self.getElements():
            result.append(''.join(itertext(element)))

        return result

    def self_text(self):
        result = Result([])
        for element in self.getElements():
            result.append(element.text)

        return result

    def html(self):
        result = Result([])
        for element in self.getElements():
            result.append(stringify_children(element))

        return result

    def attr(self, name):
        result = Result([])
        for element in self.getElements():
            try:
                result.append(element.get(name))
            except Exception as e:
                print e
                pass  # TODO: Handle errors?
        return result

    def send_keys(self, keys):
        raise NotImplementedError

    def screenshot(self, output):
        raise NotImplementedError

    def len(self):
        return len(self.elements)

    def save_to_file(self, filename):
        with codecs.open(filename, "w", "utf-8-sig") as file:
            file.write(self.html)

    def load_from_file(self, filename):
        with codecs.open(filename, "r", "utf-8-sig") as file:
            self.update(file.read())

    def raw(self):
        return self.html