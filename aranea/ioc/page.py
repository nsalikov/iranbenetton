# -*- coding: utf-8 -*-

import dependency_injector.containers as containers
import dependency_injector.providers as providers

import aranea.page.lxml_page
import aranea.page.interactive_page

page = containers.DynamicContainer()

page.lxml_page = providers.Factory(aranea.page.lxml_page.LXMLPageHandler)
page.interactive_page = providers.Factory(aranea.page.interactive_page.InteractivePageHandler)
