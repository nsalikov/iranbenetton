# -*- coding: utf-8 -*-

import dependency_injector.containers as containers
import dependency_injector.providers as providers

import aranea.drivers.phantomjs

import aranea.fetcher

from aranea.ioc.page import page

phantomjs = containers.DynamicContainer()

phantomjs.driver = providers.Singleton(aranea.drivers.phantomjs.PhantomjsDriver)

phantomjs.fetcher_lxml = providers.Factory(aranea.fetcher.Fetcher, driver=phantomjs.driver, page_handler=page.lxml_page)
phantomjs.fetcher_interactive = providers.Factory(aranea.fetcher.Fetcher, driver=phantomjs.driver, page_handler=page.interactive_page)
