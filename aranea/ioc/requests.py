# -*- coding: utf-8 -*-

import dependency_injector.containers as containers
import dependency_injector.providers as providers

import aranea.drivers.requests

import aranea.fetcher

from aranea.ioc.page import page

requests = containers.DynamicContainer()

requests.driver = providers.Singleton(aranea.drivers.requests.RequestsDriver)

requests.fetcher_lxml = providers.Factory(aranea.fetcher.Fetcher, driver=requests.driver, page_handler=page.lxml_page)
