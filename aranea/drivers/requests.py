# -*- coding: utf-8 -*-
from __future__ import absolute_import, unicode_literals

from fake_useragent import UserAgent

from scraper.proxy import random_requests_proxy
from scraper.exceptions import DownloadError
import requests
import os

ua = UserAgent()


class RequestsDriver(object):
    def __init__(self):
        self.ready = False

    def init(self):
        self.ready = True

    def get(self, url):
        proxy = random_requests_proxy()
        # print proxy
        r = requests.get(url, headers={'user-agent': ua.random}, proxies=proxy, timeout=15)
        # r = requests.get(url, headers={'user-agent': ua.random}, timeout=15)
        return r.content

    def download_file(self, url, destination, tries=3):
        print 'tries: {}'.format(tries)
        proxy = random_requests_proxy()
        # NOTE the stream=True parameter
        try:
            r = requests.get(url, stream=True, headers={'user-agent': ua.random}, proxies=proxy, timeout=15)
            # r = requests.get(url, stream=True, headers={'user-agent': ua.random}, timeout=15)
        #except requests.exceptions.ReadTimeout:
        except Exception as e:
            if tries > 0:
                return self.download_file(url, destination, tries-1)
            else:
                raise DownloadError(str(e))

        if r.status_code != 200:
            if tries > 0:
                return self.download_file(url, destination, tries-1)
            else:
                return False

        with open(destination, 'wb') as f:
            for chunk in r.iter_content(chunk_size=1024):
                if chunk:  # filter out keep-alive new chunks
                    f.write(chunk)

        statinfo = os.stat(destination)

        if statinfo.st_size == 0:
            if tries > 0:
                return self.download_file(url, destination, tries-1)
            else:
                return False

        return destination

    def is_ready(self):
        return self.ready

    def invalidate(self):
        self.ready = False
        self.quit()

    def quit(self):
        pass
