# -*- coding: utf-8 -*-
from __future__ import absolute_import, unicode_literals

from selenium import webdriver
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities
from selenium.common.exceptions import TimeoutException

from fake_useragent import UserAgent

import atexit
from scraper.proxy import random_proxy
ua = UserAgent()


class PhantomjsDriver(object):
    def __init__(self):
        self.ready = False

        #

        # proxy = '54.204.180.168:3128'
        # args += ['--proxy-auth=test:test','--proxy=' + proxy,'--proxy-type=http']


        atexit.register(self.cleanup)

    def is_ready(self):
        return self.ready

    def invalidate(self):
        self.ready = False
        self.quit()

    def init(self):
        dcap = dict(DesiredCapabilities.PHANTOMJS)
        dcap['phantomjs.page.settings.userAgent'] = ua.random
        dcap['phantomjs.page.settings.resourceTimeout'] = 15000
        args = ['--disk-cache=true', '--ignore-ssl-errors=true', '--max-disk-cache-size=100000'];
        args += ['--proxy=' + random_proxy(),'--proxy-type=http']
        # args += ['--load-images=false']

        self.driver = webdriver.PhantomJS(service_args=args, desired_capabilities=dcap)
        self.driver.implicitly_wait(15)
        self.driver.set_page_load_timeout(15)
        self.driver.set_window_size(1024, 768)
        self.ready = True

    def get(self, url, tries=3):
        try:
            self.driver.get(url)
        except TimeoutException:
            raise
        except Exception as e:
            if str(e) == '<urlopen error [Errno 111] Connection refused>':
                if tries > 0:
                    self.quit()
                    self.init()
                    return self.get(url, tries-1)
                else:
                    raise
            else:
                raise


        return self.driver.page_source

    def quit(self):
        try:
            self.driver.quit()
        except:
            pass

    def cleanup(self):
        self.quit()
        print 'cleanup here'
