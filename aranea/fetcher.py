# -*- coding: utf-8 -*-
from scraper.exceptions import DownloadError


class Fetcher(object):
    def __init__(self, driver, page_handler):
        self.driver = driver
        self.page_handler = page_handler

    def get(self, url):
        if not self.driver.is_ready():
            self.driver.init()

        try:
            result = self.driver.get(url)
        except Exception as e:
            raise DownloadError(str(e))

        self.page_handler.link_fetcher(self)
        self.page_handler.update(result, url)

        return self.page_handler

    def quit(self):
        self.driver.quit()

    def __repr__(self):
        return "Fetcher: driver {}, page_handler {}".format(self.driver, self.page_handler)
