# -*- coding: utf-8 -*-

import urllib2
import base64
from magento import MagentoAPI
import decimal
import re
import json

import requests
from requests.packages.urllib3.util.retry import Retry
from requests.adapters import HTTPAdapter


class ManegntoEstateProduct():
    def __init__(self, endpoint, port, user, key, path, manufacturer):
        self.api = MagentoAPI(endpoint, port, user, key, path)
        self.sku = None
        self.product_id = None
        self.attributes_set_id = 4
        self.status = 1
        self.manufacturer = manufacturer
        self.url = ''
        self.url_key = ''
        self.name = ''
        self.price = ''
        self.metre = ''
        self.description = ''
        self.room = ''
        self.phones = ''
        self.tel1 = ''
        self.tel2 = ''
        self.lat = ''
        self.lng = ''
        self.agent = ''
        self.websites = [1]
        self.store_view = 1
        self.magento_product = None

        self.updated = False
        self.created = False
        self.loaded = False

        self.update_log = []


    def get_category_id(self, categories):
        current_id = 2
        for category in categories:
            list = self.api.catalog_category.level(1, 1, current_id)

            tmp = 0
            for cat in list:
                if cat['name'] == category:
                    tmp = cat['category_id']

            if tmp:
                current_id = tmp
            else:
                current_id = self.api.catalog_category.create(current_id, {'name': category, 'is_active': 1,
                                                                           'available_sort_by': 'position',
                                                                           'default_sort_by': 'position',
                                                                           'include_in_menu': 1})

        return current_id

    def get_attr_id(self, attr_name, attr_value):

        if not attr_value:
            raise ValueError('attr_value is null for {}'.format(attr_name))

        info = self.api.catalog_product_attribute.info(attr_name)
        if 'options' in info:
            for option in info['options']:
                if option['label'] == attr_value:
                    return option['value']

        new_attr = {'label': [{'store_id': 0, 'value': attr_value}]}
        self.api.catalog_product_attribute.addOption(attr_name, new_attr)

        return self.get_attr_id(attr_name, attr_value)

    def _get_dict(self):
        details = dict()

        details['status'] = self.status
        # details['manufacturer'] = self.manufacturer
        details['websites'] = [1]

        details['url'] = self.url
        details['url_key'] = self.url_key
        details['name'] = self.name
        details['price'] = self.price
        details['metre'] = self.metre
        details['description'] = self.description
        details['tel1'] = self.tel1
        details['tel2'] = self.tel2
        details['lat'] = self.lat
        details['lng'] = self.lng

        if self.room:
            _id = self.get_attr_id('room', self.room)
            if _id:
                details['room'] = [_id]

        return details

    def is_same_price(self, price_name):
        if price_name not in self.__dict__:
            return False

        if price_name not in self.magento_product:
            return False

        if not self.__dict__[price_name]:
            return False

        if not self.magento_product[price_name]:
            return False

        if decimal.Decimal(self.__dict__[price_name].replace(",", ".")) == decimal.Decimal(
                self.magento_product[price_name].replace(",", ".")):
            return True
        else:
            return False

    def load(self, sku):
        sku = sku.strip() + ' '
        self.sku = sku

        try:
            self.magento_product = self.api.catalog_product.info(self.sku)
        except Exception as e:
            pass

        if self.magento_product:
            self.loaded = True
            self.product_id = self.magento_product['product_id']
            self.price = self.magento_product['price']
            self.status = self.magento_product['status']
            return True
        else:
            return False

    def save(self):
        if self.magento_product:
            self.update()
        else:
            self.create()

    def update(self):
        if self.price is None:
            self.price = ''

        if self.price.count('.') > 1:
            self.price.replace('.', '', 1)

        if not self.is_same_price('price'):
            self.api.catalog_product.update(self.sku, {'price': self.price})
            self.update_log.append({'field': 'price', 'old': self.magento_product['price'], 'new': self.price})

        if int(self.magento_product['status']) != int(self.status):
            # add log
            if self.status == 1:
                self.update_log.append({'field': 'status', 'old': 'disabled', 'new': 'enabled'})
            else:
                self.update_log.append({'field': 'status', 'old': 'enabled', 'new': 'disabled'})
            self.api.catalog_product.update(self.sku, {'status': self.status})

        if self.categories:
            new_category = self.get_category_id(self.categories)
            self.api.catalog_category.assignProduct(new_category, self.product_id)

        details = {
            'name': self.name,
            'metre': self.metre,
            'description': self.description,
            'tel1': self.tel1,
            'tel2': self.tel2,
            'lat': self.lat,
            'lng': self.lng,
        }

        if self.room:
            _id = self.get_attr_id('room', self.room)
            if _id:
                details['room'] = [_id]

        self.api.catalog_product.update(self.sku, details)

        self.updated = True

    def get_option_id(self, title):
        options = self.api.catalog_product_custom_option.list(self.magento_product['product_id'])
        for option in options:
            if option['title'] == title:
                return option['option_id']

        return 0

    def create(self):
        details = self._get_dict()
        self.product_id = self.api.catalog_product.create('simple', self.attributes_set_id, self.sku, details)

        new_category = self.get_category_id(self.categories)
        self.api.catalog_category.assignProduct(new_category, self.product_id)
        self.created = True

    def add_image(self, image_url, first_image=False):
        if image_url.startswith("//"):
            image_url = 'https:' + image_url

        img = self._get_image(image_url)
        if not img:
            return

        image = dict()
        image['file'] = dict();
        image['file']['name'] = 'image'
        image['file']['content'] = img
        image['file']['mime'] = 'image/jpeg'
        image['exclude'] = 0
        if first_image:
            image['types'] = ['image', 'small_image', 'thumbnail']

        try:
            self.api.catalog_product_attribute_media.create(self.sku, image)
        except Exception as e:
            if str(e) == "<Fault 104: 'Unsupported image format.'>":
                print 'invalid image'
            else:
                raise

    def _get_image(self, image_url):
        img = None

        s = requests.Session()
        retries = Retry(total=3, backoff_factor=1, status_forcelist=[500, 502, 503, 504])
        s.mount('http://', HTTPAdapter(max_retries=retries))
        s.mount('https://', HTTPAdapter(max_retries=retries))

        if image_url.startswith("/"):
            with open(image_url, "rb") as image_file:
                img = base64.b64encode(image_file.read())
        else:
            try:
                response = s.get(image_url, timeout=60)
                img = base64.b64encode(response.content)
            except:
                pass

        return img

    def set_types_for_first_image(self):
        mlist = self.api.catalog_product_attribute_media.list(self.product_id)
        if len(mlist) > 0:
            if len(mlist[0]['types']) == 0:
                self.api.catalog_product_attribute_media.update(self.product_id, mlist[0]['file'],
                                                                {'types': ['image', 'small_image', 'thumbnail']})

    def images_count(self):
        img_list = self.api.catalog_product_attribute_media.list(self.product_id)
        return len(img_list)

    def remove_all_images(self):
        img_list = self.api.catalog_product_attribute_media.list(self.product_id)
        for img in img_list:
            self.api.catalog_product_attribute_media.remove(self.product_id, img['file'])

    def enable(self):
        self.status = 1

    def disable(self):
        self.status = 2

    def get_all_products(self):
        return self.api.catalog_product.list(self.store_view)

    def export(self, data):
        if 'sku' in data and data['sku']:
            data['sku'] = data['sku'].strip() + ' '
        else:
            return

        self.load(data['sku'])

        if 'url' in data and data['url']:
            self.url = data['url']
        if 'sku' in data and data['sku']:
            self.url_key = data['sku']
        if 'title' in data and data['title']:
            self.name = data['title']
        if 'price' in data and data['price']:
            self.price = data['price']
        if 'details' in data and data['details']:
            self.metre = data['details']
        if 'description' in data and data['description']:
            self.description = data['description']
        if 'rooms' in data and data['rooms']:
            self.room = data['rooms']
        if 'phones' in data and data['phones']:
            self.phones = data['phones']
            try:
                self.tel1 = self.phones[0]
                self.tel2 = self.phones[1]
            except:
                pass
        if 'location' in data and data['location']:
            self.categories = data['location']
        if 'lat' in data and data['lat']:
            self.lat = data['lat']
        if 'lng' in data and data['lng']:
            self.lng = data['lng']
        # if 'agent' in data and data['agent']:
        #     self.agent = data['agent']

        self.save()

zingat = ManegntoEstateProduct('admin:a01200120@zingat.iranbenetton.com', 80, 'API User', 'bcc40ff3ef6067a0b74f24ce63bec084', '/index.php/api/xmlrpc', 'ZINGAT')
