# -*- coding: utf-8 -*-
import sys
import json

# Post should be fetched before

sys.path.append("/Users/dan/shops2")

from scraper.ioc.core import core

import scraper.drivers.koton

core.load_config()

result = scraper.drivers.koton.tools.fetch_sitemaps()

print json.dumps(result, indent=4, sort_keys=True)
