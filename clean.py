import sys
from scraper.ioc.core import core

core.load_config()

from scraper.ioc.storage import storage
from pony.orm import db_session

storage.factory()

with db_session:
    storage.factory().db.execute("delete from lists;")
    # storage.factory().db.execute("delete from products;")

from scraper.celery import app
app.control.purge()
